<?php get_header();?>
<div id='page_primary'>
<div id='page_content'>
<?php 
while(have_posts()){
?>
<?php the_post();?>
<?php get_template_part( 'content', 'page' );?>
<?php }?>
</div>
</div>
<?php get_footer();?>