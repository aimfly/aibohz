<!DOCTYPE html>
<html>
<head>
<title><?php bloginfo( 'name' );?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php 
if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );
wp_head();?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="shadow_page round_corner_5px">
<div id="header">
<div id="header_image"><img src="<?php echo get_template_directory_uri();?>/image/header_rainbow.jpg"></div>
<div id="logo"><img src="<?php echo get_template_directory_uri();?>/image/logo.png"></div>
<div id="main_menu">
<?php wp_nav_menu( array( 'theme_location' => 'primary' ) );?>
</div>
</div>
<div id='main'>