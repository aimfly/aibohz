<?php
  get_header();
?>
<div id='primary'>
<?php get_template_part('position','cat');?>
<ul id='cat_post_list'>
<?php while(have_posts()){the_post();?>
<li class='cat_post_title'><img src='<?php echo get_template_directory_uri();?>/image/list_icon.png'> <a href='<?php the_permalink();?>'>
<?php the_title();?>
</a></li>
<?php }?>
</ul>
</div><!-- end primary -->
<div style='clear:both;'></div>
<?php get_footer();?>