<?php
  get_header();
?>

<div id='primary'>
<?php get_template_part('position','post');?>
<?php 
while(have_posts()){
?>
<?php the_post();?>
<?php get_template_part( 'content', 'single' );?>
<?php }?>
</div>
<?php get_template_part('sidebar','postright');?>
<div style="clear:both"></div>
<?php get_footer();?>