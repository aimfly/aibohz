<?php

add_action( 'after_setup_theme', 'aibohz_setup' );

if(!function_exists("aibohz_setup")){
	function aibohz_setup(){
		
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 200, 200 ,true);
		add_image_size( 'single-post-thumbnail', 400, 9999 );
		add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'status', 'quote', 'image' ) );
		register_nav_menu( 'primary', __( 'Primary Menu', 'aibohz' ) );
		
	}
}

function get_next_post_by_id($id=-1,$same_cat=true){
	if(!empty($id)&&$id!=-1){
      $query = new WP_Query('p='.$id);
      if($query->have_posts()){
        
         $query->the_post();
      }
    }
    $next_post=get_next_post($same_cat);
    $ret=array();
    if(!empty($next_post)){
        $ret["id"]=$next_post->ID;
        $ret["title"]=$next_post->post_title;
        $ret["link"]=get_permalink($next_post);
    }
    wp_reset_query();
    return $ret;
}

function get_previous_post_by_id($id=-1,$same_cat=true){
	if(!empty($id)&&$id!=-1){
	  $query = new WP_Query('p='.$id);
      if($query->have_posts()){
         $query->the_post();
      }
	}
	$pre_post=get_previous_post($same_cat);
	$ret=array();
	if(!empty($pre_post)){
		$ret["id"]=$pre_post->ID;
		$ret["title"]=$pre_post->post_title;
		$ret["link"]=get_permalink($pre_post);
	}
	wp_reset_query();
	return $ret;
}

function show_header_meta(){
	printf('<img style="vertical-align:middle;" src="%2$s/image/user.png"> %1$s <img style="vertical-align:middle;" src="%3$s/image/calendar_1.png"> %4$s',
			'爱博教育',          /*hard code no good ..*/
	        get_template_directory_uri(),
	        get_template_directory_uri(),
	        get_the_date());
}

function get_cat_relationship($cat_id=-1){

    $home=array("<a title='回到首页' href='".get_home_url()."'>"."首页"."</a>");
    $cats=array();
    if($cat_id!=-1){
        $cats=split(",",get_category_parents($cat_id,true,','));
        $cats=array_filter($cats);
    }
    
    $cats=array_merge($home,$cats);
    return $cats;
}

function get_post_relationship($post_id){
	if(!empty($post_id)){
		$cats=wp_get_post_categories($post_id);
		if(!empty($cats)){
			$cat_id=$cats[0];
		}
		else{
			$cat_id=-1;
		}
	}
	else{
		$cat_id=-1;
	}
	$home=array("<a title='回到首页' href='".get_home_url()."'>"."首页"."</a>");
	$cats=array();
	if($cat_id!=-1){
		$cats=split(",",get_category_parents($cat_id,true,','));
		$cats=array_filter($cats);
	}
	
	$cats=array_merge($home,$cats);
	return $cats;
}

function aibohz_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'aibohz_excerpt_length',999 );
?>