<?php
/*
 *home page for hz aibo 
 * 
 * 
 */

get_header();?>
<div id="main_group1">
<div id="main_group1_part1">
<?php 
      $cat_obj=get_category_by_slug("activity");
      $cat_id=-1;
      if(!empty($cat_obj)){
      	$cat_id=$cat_obj->term_id;
      }
      $posts = get_posts( array('meta_key'=> 'homeshow','meta_value'=>'true','numberposts' => 1,'category' => $cat_id) ); 
      if(!empty($posts)){
      	$post=$posts[0];
      	setup_postdata($post);?>
<div class='mg1_p1_thumbnail'><?php the_post_thumbnail();?></div>
<div class='mg1_p1_des'>
<div class='mg1_p1_title entry_title main_bg_color'><a href="<?php the_permalink()?>"><?php the_title();?></a></div>
<div class='mg1_p1_contant'><?php the_content("");?></div>
</div>
<?php  }?>
</div>
<div class="vertical_split">
<div id="mg1_tip1"><img src="<?php echo get_template_directory_uri();?>/image/aboutus.png"></div>
<div id="mg1_tip2"><img src="<?php echo get_template_directory_uri();?>/image/preactive.png"></div>
</div>
<div id="main_group1_part2">
<div class='mg1_p2_content'>
<?php 

$posts = get_posts( array('post_type'=>'page','name'=>'aboutus','numberposts' => 1) );//get about us page
unset($post);
if(!empty($posts)){
	$post=$posts[0];
	setup_postdata($post);
}
?>
<a href="<?php if(isset($post)){the_permalink();}?>">
<p><span style='font-weight: bold'>爱博教育</span>，由家庭教育 “幸福三角理论”的创始人陈汉聪博士，国内户外拓展资深专家、浙江大学体验式培训师章扬，联合创立的国内首家青少年儿童体验式家庭教育机构。</p>
<p><span style='font-weight: bold'>爱博</span>：爱，即全部的接纳；博，即接纳全部！</p>
<p><span style='font-weight: bold'>爱博教育</span>，运用独立研发的“幸福三角”课程体系，结合世界最先进的体验式培训，激发孩子内在的向上的力量...<!-- hard code... --></p>
</a>
</div>
</div>
</div>
<div class='horizontal_split'>
<div class='left_rounds'></div>
<div class='right_rounds'></div>
</div>
<div id="main_group2">
<div id="main_group2_part1">
<div>
<?php 

$posts = get_posts( array('post_type'=>'page','name'=>'lesson','numberposts' => 1) );//get about us page
unset($post);
if(!empty($posts)){
    $post=$posts[0];
    setup_postdata($post);
}
?>
<a href="<?php if(isset($post)){the_permalink();}?>">
<img src='<?php echo get_template_directory_uri();?>/image/lesson_preview.jpg'>
</a>
</div>
</div>
<div class="vertical_split">
<div id="mg2_tip1"><img src="<?php echo get_template_directory_uri();?>/image/actived.png"></div>
<div id="mg2_tip2"><img src="<?php echo get_template_directory_uri();?>/image/lesson.png"></div>
</div>
<div id="main_group2_part2">
<?php 
$cat_obj=get_category_by_slug("retrospect");
$cat_id=-1;
if(!empty($cat_obj)){
	$cat_id=$cat_obj->term_id;
}
$posts = get_posts( array('meta_key'=> 'homeshow','meta_value'=>'true','numberposts' => 1,'category' => $cat_id) );
if(!empty($posts)){
	$post=$posts[0];
	setup_postdata($post);
}
?>
<div class='mg1_p1_thumbnail'><?php the_post_thumbnail();?></div>
<div class='mg1_p1_des'>
<div class='mg1_p1_title entry_title main_bg_color'><a href="<?php the_permalink()?>"><?php the_title();?></a></div>
<div class='mg1_p1_contant'><?php the_excerpt();?></div>
</div>
</div>
</div>
<div class='horizontal_split'>
<div class='left_rounds'></div>
<div class='right_rounds'></div>
<div id="mg3_tip1"><img src="<?php echo get_template_directory_uri();?>/image/team.png"></div>
</div>
<div id="main_group3">
<div id="main_group3_part1">
<div class='teacher_photo'><img style="width: 109px;" src='<?php echo get_template_directory_uri();?>/image/teacher2.jpg'></div>
<div class='teacher_des'><span style='font-weight: bold'>薛保红</span>:管理学学士，教育学硕士，在读法学博士。中国首批26位教育咨询师之一、国家二级职业指导师、心理咨询师。</div>
</div>
<div class="vertical_split"></div>
<div id="main_group3_part2">
<div class='teacher_photo'><img style="width: 109px;" src='<?php echo get_template_directory_uri();?>/image/teacher1.jpg'></div>
<div class='teacher_des'><span style='font-weight: bold'>陈汉聪</span>:华东师范大学教育学硕士，北京大学教育学博士，浙江大学教育学博士后。家庭教育“幸福三角理论”的提出者。</div>
</div>
<div class="vertical_split"></div>
<div id="main_group3_part3">
<div class='teacher_photo'><img style="width: 109px;" src='<?php echo get_template_directory_uri();?>/image/teacher3.jpg'></div>
<div class='teacher_des'><span style='font-weight: bold'>章松</span>:中国拓展行业领军人物，中国体验式培训专家，浙江大学体验式培训师，国内顶级攀岩教练，华东户外拓展著名品牌“天择”的开拓者。</div>
</div>
</div>

<?php get_footer();?>