
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry_header">
        <h1 class="entry_title"></h1>
    </div>

    <div class="entry_content">
        <?php the_content(); ?>
        <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
    </div>
</div><!-- #post-<?php the_ID(); ?> -->
