<div id='right_sidebar'>
<div id='related_read'>
<ul>
<li class='siderbar_title'>相关阅读</li>
<?php
$preview_count=3;
$prew_posts=array();
$next_posts=array();
for($i=0;$i<$preview_count;$i++){
    if($i==0){
      $prew_posts[$i]=get_previous_post_by_id();
    }else{
      $prew_posts[$i]=get_previous_post_by_id($prew_posts[$i-1]["id"]);
    }
    if(empty($prew_posts[$i]))break;
}
for($i=0;$i<$preview_count;$i++){
    if($i==0){
      $next_posts[$i]=get_next_post_by_id();
    }else{
      $next_posts[$i]=get_next_post_by_id($next_posts[$i-1]["id"]);
    }
    if(empty($next_posts[$i]))break;
}

/*print preview link*/
for($i=count($prew_posts)-1;$i>-1;$i--){
    if(!empty($prew_posts[$i])){
        printf('<li class="sidebar_post_title"><a href="%1$s">%2$s</a></li>',$prew_posts[$i]["link"],$prew_posts[$i]["title"]);
    }
}
echo '<li class="sidebar_cur_post_title"><a href="#">';
the_title();
echo '</a><span class="cur_title_tip"></span>';
echo '</li>';
for($i=0;$i<count($next_posts);$i++){
    if(!empty($next_posts[$i])){
        printf('<li class="sidebar_post_title"><a href="%1$s">%2$s</a></li>',$next_posts[$i]["link"],$next_posts[$i]["title"]);
    }
}?>
</ul>
</div>
</div>