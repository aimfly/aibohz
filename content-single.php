<div class='single_post'>
<div class='entry_header'>
<h1 class='entry_title'><?php the_title();?></h1>
<div class='entry_header_meta'><?php show_header_meta();?></div>
</div>
<div class='entry_content'><?php the_content();?></div>
</div>